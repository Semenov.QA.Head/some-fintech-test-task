from dataclasses import dataclass
from typing import Tuple, List

import pytest

from order_book import OrderBook, OrderId


@dataclass()
class OrderForTests:
    order_type: str
    price: float
    quantity: int
    flag: bool

    @property
    def order_params(self) -> dict:
        return dict(order_type=self.order_type, price=self.price, quantity=self.quantity)


@pytest.fixture(autouse=True)
def the_book():
    return OrderBook('BKNG')


def add_and_remove_orders(book: OrderBook, orders: List[OrderForTests]) \
                    -> List[Tuple[OrderId, OrderForTests]]:
    """
    Adds (and remove) orders to (from) a given order book.

    Args:
        book: Given order book
        orders: List of OrderForTests. All items will be added to order book.
                After that items with flag==True will be removed from order book.

    Returns:
        List of tuples (OrderId, OrderForTests)
    """

    order_ids = [book.add_order(**item.order_params) for item in orders]
    assert all(order_ids)

    result = list(zip(order_ids, orders))

    deleted = [book.remove_order(item[0]) for item in result if item[1].flag]
    assert all(deleted)

    return result


@pytest.mark.parametrize(
    ids=[
        'symbol-available-for-order-book',
        'different-order-books-may-has-different-symbols',
    ],
    argnames='symbols',
    argvalues=[
        [['ONE']],
        [['ONE', 'ANOTHER-ONE']],
    ]
)
def test_order_book_symbol(symbols):
    books = []
    for item in symbols:
        books.append(OrderBook(item))
    for i, item in enumerate(books):
        assert item.symbol == symbols[i]


@pytest.mark.parametrize(
    ids=[
        'ask-order-added',
        'bid-order-added',
        'bad-order-type-was-not-added'],
    argnames='sample',
    argvalues=[
        OrderForTests('ask', 13.7654, 20, True),
        OrderForTests('bid', 140, 8, True),
        OrderForTests('bad', 13.7654, 8, False),
    ]
)
def test_add_order(the_book, sample):
    order_id = the_book.add_order(**sample.order_params)
    if sample.flag:
        assert order_id is not None
    else:
        assert order_id is None


@pytest.mark.parametrize(
    ids=[
        'ask-order-type',
        'bid-order-type',
    ],
    argnames='sample',
    argvalues=[
        OrderForTests('ask', 13.7654, 20, True),
        OrderForTests('bid', 140, 8, True),
    ]
)
def test_order_info(the_book, sample):
    order_id = the_book.add_order(**sample.order_params)
    assert the_book.order_info(order_id) == sample.order_params


@pytest.mark.parametrize(
    ids=[
        'many-ask-and-bid',
        'many-ask-and-bid-after-removes',
        'all-added-orders-was-removed',
        'with-price-duplicates'
        ],
    argnames='orders',
    argvalues=[
        [
            OrderForTests('ask', 76.4, 120, False),
            OrderForTests('ask', 12.74, 9, False),
            OrderForTests('bid', 1005, 2, False),
            OrderForTests('bid', 201.40, 78, False),
        ],
        [
            OrderForTests('ask', 76.4, 120, True),
            OrderForTests('ask', 102.74, 9, False),
            OrderForTests('ask', 12.74, 9, False),
            OrderForTests('bid', 1005, 2, False),
            OrderForTests('bid', 201.40, 78, True),
            OrderForTests('bid', 15, 2, False),
        ],
        [
            OrderForTests('ask', 76.4, 120, True),
            OrderForTests('ask', 12.74, 9, True),
            OrderForTests('bid', 1005, 2, True),
            OrderForTests('bid', 201.40, 78, True),
        ],
        [
            OrderForTests('ask', 12.4, 2, False),
            OrderForTests('ask', 7.62, 120, False),
            OrderForTests('ask', 102.74, 9, True),
            OrderForTests('ask', 7.62, 5, False),
            OrderForTests('bid', 15.1, 2, False),
            OrderForTests('bid', 201.40, 78, False),
            OrderForTests('bid', 12.4, 2, True),
            OrderForTests('bid', 15.1, 20, False),
        ],
    ]
)
def test_multiple_orders_info(the_book, orders):

    for record in add_and_remove_orders(the_book, orders):
        order_id, order = record
        if order.flag:
            assert the_book.order_info(order_id) is None
        else:
            assert the_book.order_info(order_id) == order.order_params


def test_remove_order_invalid_id(the_book):
    assert the_book.remove_order(100) is None, "Invalid id. Result should be None!"


def test_attempt_to_remove_order_twice(the_book):
    order_id = the_book.add_order(**OrderForTests('ask', 203.7, 3, True).order_params)
    assert the_book.remove_order(order_id), "Valid id. Result should be True!"
    assert the_book.remove_order(order_id) is None, "Invalid id. Result should be None!"


@pytest.mark.parametrize(
    ids=[
            'empty-order-book',
            'many-ask-and-bid',
            'many-ask-and-bid-after-removes',
            'all-added-orders-was-removed',
            'no-consolidation-of-orders'
        ],
    argnames='orders',
    argvalues=[
        [],
        [
            OrderForTests('ask', 76.4, 120, False),
            OrderForTests('ask', 12.74, 9, False),
            OrderForTests('bid', 1005, 2, False),
            OrderForTests('bid', 201.40, 78, False),
        ],
        [
            OrderForTests('ask', 76.4, 120, True),
            OrderForTests('ask', 102.74, 9, False),
            OrderForTests('ask', 12.74, 9, False),
            OrderForTests('bid', 1005, 2, False),
            OrderForTests('bid', 201.40, 78, True),
            OrderForTests('bid', 15, 2, False),
        ],
        [
            OrderForTests('ask', 76.4, 120, True),
            OrderForTests('ask', 12.74, 9, True),
            OrderForTests('bid', 1005, 2, True),
            OrderForTests('bid', 201.40, 78, True),
        ],
        [
            OrderForTests('ask', 12.4, 2, False),
            OrderForTests('ask', 7.62, 120, False),
            OrderForTests('ask', 102.74, 9, True),
            OrderForTests('ask', 7.62, 5, False),
            OrderForTests('bid', 15.1, 2, False),
            OrderForTests('bid', 201.40, 78, False),
            OrderForTests('bid', 12.4, 2, True),
            OrderForTests('bid', 15.1, 20, False),
        ],
    ]
)
def test_as_dict(the_book, orders):
    all_data = sorted(filter(lambda x: not x.flag, orders), key=lambda x: x.price)
    expected = {'ask': [], 'bid': []}
    for item in all_data:
        expected[item.order_type].append(dict(price=item.price, quantity=item.quantity))

    add_and_remove_orders(the_book, orders)
    actual_data = the_book.as_dict()

    assert isinstance(actual_data, dict)
    assert actual_data == expected


def test_as_dict_is_safe_from_mutations(the_book):
    orders = [
        OrderForTests('ask', 76.4, 120, False),
        OrderForTests('ask', 12.74, 9, False),
        OrderForTests('bid', 1005, 2, False),
        OrderForTests('bid', 201.40, 78, False),
    ]

    expected_1 = {'ask': [], 'bid': []}
    for item in sorted(orders, key=lambda x: x.price):
        expected_1[item.order_type].append(dict(price=item.price, quantity=item.quantity))

    # get actual values
    records = add_and_remove_orders(the_book, orders)
    actual_1 = the_book.as_dict()

    for item in (0, ~0):
        orders.pop(item)
        # make changes in order book
        order_id, _ = records.pop(item)
        the_book.remove_order(order_id)

    expected_2 = {'ask': [], 'bid': []}
    for item in sorted(orders, key=lambda x: x.price):
        expected_2[item.order_type].append(dict(price=item.price, quantity=item.quantity))

    # get new actual values
    actual_2 = the_book.as_dict()

    assert actual_1 == expected_1, "New call of as_dict method corrupt data of previous calls!"
    assert actual_2 == expected_2, "Not relevant data in second call of as_dict method!"


def test_as_dict_on_many_instances():
    books = [OrderBook('ONE'), OrderBook('TWO')]
    orders = [
        [
            OrderForTests('ask', 76.4, 120, False),
            OrderForTests('ask', 12.74, 9, False),
            OrderForTests('bid', 1005, 2, False),
            OrderForTests('bid', 201.40, 78, False),
        ],
        [
            OrderForTests('ask', 76.4, 120, True),
            OrderForTests('ask', 102.74, 9, False),
            OrderForTests('ask', 12.74, 9, False),
            OrderForTests('bid', 1005, 2, False),
            OrderForTests('bid', 201.40, 78, True),
            OrderForTests('bid', 15, 2, False),
        ]
    ]

    expected = [{'ask': [], 'bid': []}, {'ask': [], 'bid': []}]
    actual = []

    for i in range(len(expected)):
        for item in sorted(filter(lambda x: not x.flag, orders[i]), key=lambda x: x.price):
            expected[i][item.order_type].append(dict(price=item.price, quantity=item.quantity))

        add_and_remove_orders(books[i], orders[i])
        actual.append(books[i].as_dict())

    assert expected[0] != expected[1], "simple self check"

    for i in range(len(expected)):
        assert isinstance(actual[i], dict)
        assert actual[i] == expected[i]
