from itertools import count, groupby
from typing import NewType, Optional


OrderId = NewType('OrderId', int)


class OrderBook:
    """
    Simple order book implementation for test design practice.

    WARNING!!
        Don't use this one in really projects! At least (but not limited):
        1. There is no consolidation of orders with same prices.
        2. You need to use `decimals` type for price instead of `float`.
    """

    def __init__(self, symbol):
        """

        Args:
            symbol: Alias of the trading instrument.
        """
        self.symbol = symbol
        self._data = dict()
        self._id = count(1)

    def add_order(self, order_type: str, price: float, quantity: int) -> Optional[OrderId]:
        """
        Add an order to order book

        Args:
            order_type: Type of order. Maybe either 'ask' or 'bid'.
            price: Desired price.
            quantity: Quantity.

        Returns:
            order identifier
        """
        if order_type not in ('ask', 'bid'):
            return
        order_id = next(self._id)
        self._data[order_id] = dict(order_type=order_type,
                                    price=price,
                                    quantity=quantity)
        return OrderId(order_id)

    def remove_order(self, order_id: OrderId) -> Optional[bool]:
        """
        Remove order from order book by specified order_id.

        Args:
            order_id: Id of the order which will be removed.

        Returns:
            True if order removed successfully.
            None in any other cases.
        """
        if order_id in self._data:
            del self._data[order_id]
            return True

    def order_info(self, order_id: OrderId) -> Optional[dict]:
        """
        Return info about order by specified order_id.

        Args:
            order_id: Id of the order which info will be returned.

        Returns:
            Dict with order info if order with given order_id exists.
            None in any other cases.
        """
        return self._data.get(order_id)

    def as_dict(self) -> dict:
        """
        Return info about all existing orders as a dict with following structure:

        ```
        {
            "asks": [
                {
                    "price": <value>,
                    "quantity": <value>
                },
            ...
            ],
            "bids": [...]
        }
        ```

        Orders are sorted by price.

        Returns:
            Dict with info about all existing orders.
        """

        result = {'ask': [], 'bid': []}
        data = sorted(self._data.values(), key=lambda x: x['price'])

        for group, item_iter in groupby(data, lambda x: x['order_type']):
            orders = [{'price': item['price'], 'quantity': item['quantity']}
                      for item in item_iter]
            result[group].extend(orders)

        return result
