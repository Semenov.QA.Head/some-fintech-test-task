Description
=====

The test task for a qualification to a nice company.
Task description you can see in `specification.md`


Prerequisites
-----

For running this project you need python-3.7 or above.


Running tests
-----

First of all install the dependencies: ```pip install -r requirements-test.txt```

For view check-list use ```pytest --collect-only```

For run test use ```pytest -vv``` 
